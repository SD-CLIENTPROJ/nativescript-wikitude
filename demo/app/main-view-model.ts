import { Observable } from 'tns-core-modules/data/observable';
import { Wikitude, ScreenCaptureEventData } from 'nativescript-wikitude';
import { saveToAlbum } from './helper';

export class HelloWorldModel extends Observable {
  public message: string;
  //private wikitude: Wikitude;

  public wikitudeInstance: Wikitude;

  constructor() {
    super();
  }

  takeScreenShot() {
    if ( this.wikitudeInstance ) {
      this.wikitudeInstance.captureScreen(true);
    }
  }

  switchCamera() {
    if ( this.wikitudeInstance ) {
      this.wikitudeInstance.switchCamera();
    }
  }

  toggleFlash() {
    if ( this.wikitudeInstance ) {
      this.wikitudeInstance.toggleFlash();  
    }
  }

  onWorldLoad($event) {
    console.log('wikitude AR World Loaded');
    this.wikitudeInstance = $event.object;
  }


  onScreenShot($event: ScreenCaptureEventData) {
    let isSuccessful = true;
    try {
      saveToAlbum($event.data, 'png');
    } catch(e) {
      isSuccessful = false;
      alert('Error Saving Screenshot: ' + e.message);
    } finally {
      alert( isSuccessful ? 'Screenshot saved ' : 'Screenshot not saved!');
    }
  }

  onScreenShotFailed($event) {
    console.log('An Error Occured while taking a screenshot: ' + $event.data.error.message);
  }

}
