import { Observable } from 'tns-core-modules/data/observable';
import { Permissions } from '@spartadigital/nativescript-permissions';

import { topmost } from 'tns-core-modules/ui/frame';


export class PermissionsViewModel extends Observable {

  get hasRequiredPermissions(): boolean {
    return this.get('hasCameraPermissions') && this.get('hasLocationPermissions');
  }

  constructor() {
    super();

    this.set('hasLocationPermissions', Permissions.hasLocationPermission());
    this.set('hasCameraPermissions', Permissions.hasCameraPermission());
    this.set('hasStoragePermissions', Permissions.hasFilePermission());
  }

  requestLocationPermissions() {
    Permissions.requestLocationPermission()
      .then((hasPermission) => this.set('hasLocationPermissions', hasPermission));
  }

  requestCameraPermissions() {
    Permissions.requestCameraPermission()
      .then((hasPermission) => this.set('hasCameraPermissions', hasPermission));
  }

  requestStoragePermissions() {
    Permissions.requestFilePermission()
      .then((hasPermission) => this.set('hasStoragePermissions', hasPermission));
  }

  navigateToWikitudeViewer() {
    const navigationEntry = {
      moduleName: "main-page",
      context: { info: "something you want to pass to your page" },
      animated: true
    };

    topmost().navigate(navigationEntry);
  }

}