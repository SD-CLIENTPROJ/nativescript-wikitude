if ((<any>global).TNS_WEBPACK) {
    require("tns-core-modules/bundle-entry-points");

    global.registerModule("permissions-page", () => require("./permissions-page"));
    global.registerModule("main-page", () => require("./main-page"));
}