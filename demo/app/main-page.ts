import * as observable from 'tns-core-modules/data/observable';
import * as pages from 'tns-core-modules/ui/page';
import {HelloWorldModel} from './main-view-model';

import { Wikitude } from 'nativescript-wikitude';

(global as any).wikitudeLicense = "jmz+Sqz/9EKlanFQzIQLSX+FouBhZ7grlwvpxtnuhjaIM4fjhcT9BDsjd/X6T4onxi33GA7uTD/CpfcfUZQqrgmJCggbuQ5gwjZ2RBAvoLADhUizJPRxEE+rpF4B7Mu8xTQSB3+5QewoSWUzAxqElbkmNKSGOuVy1EiNL33Jmv1TYWx0ZWRfX3WJpRPN4Lsf5zqSa2jk3YPMv5F/g7G7RoIlRt0ErovWXxa+LLfDNH8c1dAWT+Tt2iXUj9cX119lG+VOw/yxrAcOeaLgjR2RIDhYzH+nhCWdoGFkhJA36ZoMy27KEypN/RaPVzHwhZltWoRSyWIl/dww+Q+C91GkzeNd//v9MtNXLWyw0MVy0vBz3f2+iIop5fXeZWB4q+p/WpXNXXUgHXFz1KvuJiQQ+nzWqzV3gWxDFk3ivBWKeDuRsFkSZ0x/9UlwIoitVKmFiLSWcFsbM0wbMwI7L59vR3jbXOGNkDFYO7RWxD1hmCWNZIvTbLzZxPbvU/913KqFoplmwSKpplizenSXlZYKu8f/kNGHeOgiO5COog4cSuDlog7a9/ucbjOXBdksCsaoqHBgl66QYV+5LUvTyR8V0MOSn7aR7xuABE6yhLDIgif0Lnj3oi+hfs4Im3h/ZCbFcsa0LtaS5xxz1VJQ/lzrrXQeeK14H0UT2owy8vFbpeA=";

// Event handler for Page 'loaded' event attached in main-page.xml
export function pageLoaded(args: observable.EventData) {
    // Get the event sender
    let page = <pages.Page>args.object;
    page.bindingContext = new HelloWorldModel();

}
